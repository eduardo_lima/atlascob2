package br.com.atlasco.enums;

/**
 * Created by home on 06/06/2015.
 */
public enum ActionEnum {

    MARK_GO_CUSTOMER(1L), MARK_GO_START_SERVICE(2L), MARK_GO_END_SERVICE(3L), MARK_GO_HOME(4L), MARK_NO_ACTION(5L);

    public long value;
    ActionEnum(long l) {
        value = l;
    }

    public long getValue(){
        return value;
    }
}
