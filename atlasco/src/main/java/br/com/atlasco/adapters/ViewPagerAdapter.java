package br.com.atlasco.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import br.com.atlasco.fragments.TabMapFragment;
import br.com.atlasco.fragments.TabNotesFragment;
import br.com.atlasco.fragments.TabStatisticsFragment;

/**
 * Created by home on 05/06/2015.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private CharSequence mTitles[]; // This will Store the mTitles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    private int mNumOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


    // Build a Constructor and assign the passed Values to appropriate values in the class
    public ViewPagerAdapter(FragmentManager fm,CharSequence mTitles[], int mNumbOfTabsumb) {
        super(fm);

        this.mTitles = mTitles;
        this.mNumOfTabs = mNumbOfTabsumb;

    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        if(position == 0) // if the position is 0 we are returning the First tab
        {
            TabMapFragment tab1 = new TabMapFragment();
            return tab1;
        }
        else if (position == 1)           // As we are having 2 tabs if the position is now 0 it must be 1 so we are returning second tab
        {
            TabStatisticsFragment tab2 = new TabStatisticsFragment();
            return tab2;
        }
        else if (position == 2)
        {
            TabNotesFragment tab3 = new TabNotesFragment();
            return tab3;
        }
        return null;
    }

    // This method return the mTitles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles[position];
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
