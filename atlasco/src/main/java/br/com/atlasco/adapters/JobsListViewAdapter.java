package br.com.atlasco.adapters;

import android.content.Context;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.Collection;
import java.util.List;

import br.com.atlasco.R;
import br.com.atlasco.db.model.Job;

/**
 * Created by home on 14/06/2015.
 */
public class JobsListViewAdapter extends ArrayAdapter<Job> {

    // Declare Variables
    private final Context context;
    private final LayoutInflater inflater;
    private List<Job> mJobList;
    private SparseBooleanArray mSelectedItemsIds;

    public JobsListViewAdapter(Context context, int resourceId,
                               List<Job> list) {
        super(context, resourceId, list);
        mSelectedItemsIds = new SparseBooleanArray();
        this.context = context;
        this.mJobList = list;
        inflater = LayoutInflater.from(context);
    }

    private class ViewHolder {
        TextView description;
        TextView createdAt;
        TextView status;
    }

    public View getView(int position, View view, ViewGroup parent) {

        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.item_list_jobs, null);
            // Locate the TextViews in item_list_jobs.xml
            holder.description = (TextView) view.findViewById(R.id.description);
            holder.createdAt = (TextView) view.findViewById(R.id.created_at);
            holder.status = (TextView) view.findViewById(R.id.sub_description);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        view.setSelected(mSelectedItemsIds.get(position) ? true : false);

        // Capture position and set to the TextViews
        holder.description.setText(mJobList.get(position).getDescription());
        holder.createdAt.setText(new DateTime(mJobList.get(position).getCreatedAt()).toString(DateTimeFormat.forPattern("dd 'de' MMMM 'de' yyyy")));
        holder.status.setText(mJobList.get(position).getStatus());

        return view;
    }

    @Override
    public void remove(Job object) {
        mJobList.remove(object);
        notifyDataSetChanged();
    }

    @Override
    public void addAll(Collection<? extends Job> collection) {
        mJobList.clear();
        mJobList.addAll(collection);
        notifyDataSetChanged();
    }

    public Job getJobAtPosition(int position){
        return mJobList.get(position);
    }

    public List<Job> getJobList() {
        return mJobList;
    }

    public void toggleSelection(int position) {
        selectView(position, !mSelectedItemsIds.get(position));
    }

    public void removeSelection() {
        mSelectedItemsIds = new SparseBooleanArray();
        notifyDataSetChanged();
    }

    public void selectView(int position, boolean value) {
        if (value)
            mSelectedItemsIds.put(position, value);
        else
            mSelectedItemsIds.delete(position);
        notifyDataSetChanged();
    }

    public int getSelectedCount() {
        return mSelectedItemsIds.size();
    }

    public SparseBooleanArray getSelectedIds() {
        return mSelectedItemsIds;
    }
}
