package br.com.atlasco.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.Collection;
import java.util.List;

import br.com.atlasco.R;
import br.com.atlasco.db.model.Note;

/**
 * Created by Eduardo on 19/07/2015.
 */
public class NotesListViewAdapter extends ArrayAdapter<Note> {

    // Declare Variables
    private final Context mContext;
    private final LayoutInflater mInflater;
    private List<Note> mNotesList;


    public NotesListViewAdapter(Context context, int resource, List<Note> objects) {
        super(context, resource, objects);

        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        mNotesList = objects;
    }


    private class ViewHolder {
        TextView description;
    }

    public View getView(int position, View view, ViewGroup parent) {

        final ViewHolder holder;
        if (view == null) {
            holder = new ViewHolder();
            view = mInflater.inflate(R.layout.item_list_notes, null);
            // Locate the TextViews in item_list_jobs.xml
            holder.description = (TextView) view.findViewById(R.id.note_description);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        // Capture position and set to the TextViews
        holder.description.setText(mNotesList.get(position).getDescription());

        return view;
    }

    @Override
    public void remove(Note object) {
        mNotesList.remove(object);
        notifyDataSetChanged();
    }

    @Override
    public void addAll(Collection<? extends Note> collection) {
        mNotesList.clear();
        mNotesList.addAll(collection);
        notifyDataSetChanged();
    }

    public Note getNoteAtPosition(int position){
        return mNotesList.get(position);
    }

    public List<Note> getJobList() {
        return mNotesList;
    }
}
