package br.com.atlasco.db.actions;


import android.content.ContentValues;
import android.content.Context;

import br.com.atlasco.R;
import br.com.atlasco.db.utils.Consts;

/**
 * Action to create new notes for the jobs
 * Created by Eduardo on 21/07/2015.
 */
public class DoNewNote extends ActionBase {

    private static final String TAG = DoNewNote.class.getSimpleName();

    private final Context mContext;
    private final long mJobId;
    private final String mDescription;

    /**
     * Creates an object to help inserting, selecting and/or deleting rows
     *
     * @param c Context of the application
     */
    public DoNewNote(Context c, long jobId, String d) {
        super(c);

        mContext = c;
        mJobId =jobId;
        mDescription = d;
    }

    @Override
    protected void validate() throws Exception {
        if (mDescription.isEmpty()){
            throw new Exception(mContext.getString(R.string.message_description_not_set));
        }
    }

    @Override
    protected void execute() throws Exception {

        ContentValues values = new ContentValues();
        values.put(Consts.KEY_JOB_ID, mJobId);
        values.put(Consts.KEY_DESCRIPTION, mDescription);
        this.getDatabase().insert(Consts.TABLE_NOTES, null, values);
    }
}
