package br.com.atlasco.db.actions;

import android.content.Context;
import android.database.Cursor;

import java.util.ArrayList;
import java.util.List;

import br.com.atlasco.db.model.Note;
import br.com.atlasco.db.utils.Consts;

/**
 * Action to get all notes of the job
 * Created by Eduardo on 21/07/2015.
 */
public class DoGetAllNotes extends ActionBase{

    private static final String TAG = DoGetAllNotes.class.getSimpleName();

    private final long mJobID;

    private List<Note> mList;

    public DoGetAllNotes(Context c, long jobID) {
        super(c);

        mJobID = jobID;
        this.mList = new ArrayList<Note>();
    }

    @Override
    protected void execute() throws Exception {

        String selectQuery = "SELECT " + Consts.TABLE_NOTES + "." + Consts.KEY_ID + " C1, " + Consts.TABLE_NOTES + "."
                + Consts.KEY_DESCRIPTION + " C2 FROM " + Consts.TABLE_NOTES + " WHERE " + Consts.TABLE_NOTES + "."
                + Consts.KEY_JOB_ID + " = " + mJobID;

        Cursor cursor = this.getDatabase().rawQuery(selectQuery, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Note n = new Note();
            n.setId(cursor.getInt(cursor.getColumnIndex("C1")));
            n.setJobId(mJobID);
            n.setDescription(cursor.getString(cursor.getColumnIndex("C2")));

            mList.add(n);

            cursor.moveToNext();
        }
    }

    public List<Note> getmList() {
        return mList;
    }

}
