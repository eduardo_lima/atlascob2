package br.com.atlasco.db.model;

/**
 * Contains information of actions
 * 
 * @author eduardo-lima
 *
 */
public class Action {

	private long id;
	private String description;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
}
