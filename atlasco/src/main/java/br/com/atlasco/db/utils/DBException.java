package br.com.atlasco.db.utils;

/**
 * When something not expected in DB operations occurs
 * @author eduardo-lima
 *
 */
public class DBException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DBException(String msg){
		super(msg);
	}
}
