package br.com.atlasco.db.actions;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import br.com.atlasco.R;
import br.com.atlasco.db.utils.Consts;

/**
 * Created by home on 19/06/2015.
 */
public class DoDeleteJob extends ActionBase {

    private final String TAG = DoDeleteJob.class.getSimpleName();

    private final long mJobID;

    private final Context mContext;

    /**
     * Creates an object to help inserting, selecting and/or deleting rows
     *
     * @param c Context of the application
     */
    public DoDeleteJob(Context c, long id) {
        super(c);

        Log.i(TAG, "Initializing DoDeleteJob");

        this.mJobID = id;
        mContext = c;
    }

    @Override
    protected void validate() throws Exception {

        String selectQuery = "SELECT " + Consts.KEY_ID + ", "+ Consts.KEY_DESCRIPTION + ", " + Consts.KEY_STATUS + " FROM " + Consts.TABLE_JOBS +
                " WHERE " + Consts.KEY_ID + " = ?";

        String[] where = new String[1];
        where[0] = Long.toString(mJobID);

        Cursor cursor = this.getDatabase().rawQuery(selectQuery, where);

        if (cursor.moveToFirst()){
            if (!cursor.getString(cursor.getColumnIndex(Consts.KEY_STATUS)).equals(mContext.getString(R.string.status_job_done))){
                throw new Exception(mContext.getString(R.string.toast_cannot_delete) + " " + cursor.getString(cursor.getColumnIndex(Consts.KEY_DESCRIPTION)));
            }
        }
    }

    @Override
    protected void execute() throws Exception {

        Log.i(TAG, "Removing job...");

        this.getDatabase().delete(Consts.TABLE_JOBS, Consts.KEY_ID + " = " + mJobID, null);
    }
}
