package br.com.atlasco.db.model;

/**
 * Contains information of tracks
 * 
 * @author eduardo-lima
 *
 */
public class Track {
	
	private long id;
	private long jobId;
	private float distanceTraveled;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getJobId() {
		return jobId;
	}
	public void setJobId(long jobId) {
		this.jobId = jobId;
	}
	public float getDistanceTraveled() {
		return distanceTraveled;
	}
	public void setDistanceTraveled(float distanceTraveled) {
		this.distanceTraveled = distanceTraveled;
	}
}
