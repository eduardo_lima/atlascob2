package br.com.atlasco.db.actions;

import java.util.ArrayList;
import java.util.List;

import br.com.atlasco.db.model.Location;
import br.com.atlasco.db.utils.Consts;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

/**
 * Get locations of all track
 * 
 * @author eduardo-lima
 *
 */
public class DoGetAllLocation extends ActionBase {

	private static final String TAG = DoGetAllLocation.class.getSimpleName();

	private long mTrackID;

	private List<Location> mList;

	/**
	 * Init with track id and context
	 * 
	 * @param c
	 * @param id
	 *            Track id
	 */
	public DoGetAllLocation(Context c, long id) {
		super(c);

		Log.i(TAG, "Initializing " + DoGetAllLocation.class.getSimpleName());

		mTrackID = id;

		mList = new ArrayList<Location>();
	}

	@Override
	protected void execute() throws Exception {

		Log.i(TAG, "CGetting tracks of job " + mTrackID);

		String[] args = { Long.toString(mTrackID) };
		Cursor cursor = this.getDatabase().query(Consts.TABLE_LOCATIONS, null, Consts.KEY_TRACK_ID + " = ?", args, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Location l = new Location();
			l.setId(cursor.getLong(cursor.getColumnIndex(Consts.KEY_ID)));
			l.setTrackId(cursor.getLong(cursor.getColumnIndex(Consts.KEY_TRACK_ID)));
			l.setLatitude(cursor.getDouble(cursor.getColumnIndex(Consts.KEY_LATITUDE)));
			l.setLongitude(cursor.getDouble(cursor.getColumnIndex(Consts.KEY_LONGITUDE)));
			
			this.mList.add(l);

			cursor.moveToNext();
		}
	}

	public List<Location> getmList() {
		return mList;
	}

}
