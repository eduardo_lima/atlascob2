package br.com.atlasco.db.actions;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import br.com.atlasco.R;
import br.com.atlasco.db.utils.Consts;

/**
 * Created by home on 20/06/2015.
 */
public class DoDeleteMarkings extends ActionBase {

    private final static String TAG = DoDeleteMarkings.class.getSimpleName();

    private final long mJobID;

    private final Context mContext;

    /**
     * Creates an object to help inserting, selecting and/or deleting rows
     *
     * @param c Context of the application
     */
    public DoDeleteMarkings(Context c, long id) {
        super(c);

        this.mJobID = id;
        mContext = c;
    }

    @Override
    protected void validate() throws Exception {

        String selectQuery = "SELECT " + Consts.KEY_ID + ", "+ Consts.KEY_DESCRIPTION + ", " + Consts.KEY_STATUS + " FROM " + Consts.TABLE_JOBS +
                " WHERE " + Consts.KEY_ID + " = ?";

        String[] where = new String[1];
        where[0] = Long.toString(mJobID);

        Cursor cursor = this.getDatabase().rawQuery(selectQuery, where);

        if (cursor.moveToFirst()){
            if (!cursor.getString(cursor.getColumnIndex(Consts.KEY_STATUS)).equals(mContext.getString(R.string.status_job_done))){
                throw new Exception("Não foi possível remover " + cursor.getString(cursor.getColumnIndex(Consts.KEY_DESCRIPTION)));
            }
        }
    }

    @Override
    protected void execute() throws Exception {
        String selectQuery = "SELECT " + Consts.TABLE_MARKINGS + "." + Consts.KEY_ID + " FROM " + Consts.TABLE_MARKINGS +
                " WHERE " + Consts.TABLE_MARKINGS + "." + Consts.KEY_JOB_ID + " = " + mJobID;

        Cursor cursor = this.getDatabase().rawQuery(selectQuery, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()){

            this.getDatabase().delete(Consts.TABLE_MARKINGS, Consts.KEY_ID + " = " + cursor.getLong(cursor.getColumnIndex(Consts.KEY_ID)), null);

            cursor.moveToNext();
        }
    }
}
