package br.com.atlasco.db.actions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import br.com.atlasco.db.model.Job;
import br.com.atlasco.db.utils.Consts;

/**
 * Get all jobs from db
 * 
 * @author eduardo-lima
 *
 */
public class DoGetAllJob extends ActionBase {

	private static final String TAG = "DoGetAllJob";

	private List<Job> mList;

	public List<Job> getmList() {
		return mList;
	}

	/**
	 * Instanciate the list of Jobs
	 * 
	 * @param c
	 *            Context of the application
	 */
	public DoGetAllJob(Context c) {
		super(c);

		Log.i(TAG, "Initializing..");

		this.mList = new ArrayList<Job>();
	}

	@Override
	protected void execute() throws Exception {

		Log.i(TAG, "Getting all jobs...");

		String selectQuery = "SELECT " + Consts.TABLE_JOBS + "." + Consts.KEY_ID + " C1, " + Consts.TABLE_JOBS + "."
				+ Consts.KEY_DESCRIPTION + " C2, " + Consts.TABLE_JOBS + "." + Consts.KEY_CREATED_AT + " C3, " + Consts.TABLE_JOBS + "."
				+ Consts.KEY_STATUS + " C4  FROM " + Consts.TABLE_JOBS + " ORDER BY C1 DESC";

		Cursor cursor = this.getDatabase().rawQuery(selectQuery, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Job j = new Job();
			j.setId(cursor.getInt(cursor.getColumnIndex("C1")));
			j.setDescription(cursor.getString(cursor.getColumnIndex("C2")));
			j.setStatus(cursor.getString(cursor.getColumnIndex("C4")));

			try {
				j.setCreatedAt(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(cursor.getString(cursor
						.getColumnIndex("C3"))));
			} catch (ParseException e) {
				// silent
			}
			this.mList.add(j);

			cursor.moveToNext();
		}

	}
}
