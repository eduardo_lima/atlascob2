package br.com.atlasco.db.model;

/**
 * Created by Eduardo on 19/07/2015.
 */
public class Note {

    private long id;
    private long jobId;
    private String description;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getJobId() {
        return jobId;
    }

    public void setJobId(long jobId) {
        this.jobId = jobId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
