package br.com.atlasco.db.actions;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import br.com.atlasco.db.model.Action;
import br.com.atlasco.db.utils.Consts;

/**
 * Created by home on 13/06/2015.
 */
public class DoGetAction extends ActionBase {

    private static final String TAG = DoGetAction.class.getSimpleName();

    private final long mActionID;

    private Action mAction;

    /**
     * Creates an object to help inserting, selecting and/or deleting rows
     *
     * @param c Context of the application
     */
    public DoGetAction(Context c, long id) {
        super(c);

        Log.i(TAG, "Initializing...");

        mActionID = id;
    }

    @Override
    protected void execute() throws Exception {
        Log.i(TAG, "Getting actions...");

        String selectQuery = "SELECT " + Consts.TABLE_ACTIONS + "." + Consts.KEY_ID + " C1, " + Consts.TABLE_ACTIONS + "."
                + Consts.KEY_DESCRIPTION + " C2 FROM " + Consts.TABLE_ACTIONS + " WHERE C1 = " + mActionID;

        Cursor cursor = this.getDatabase().rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            mAction = new Action();
            getmAction().setId(cursor.getLong(cursor.getColumnIndex("C1")));
            getmAction().setDescription(cursor.getString(cursor.getColumnIndex("C2")));
        }

    }

    public Action getmAction() {
        return mAction;
    }
}
