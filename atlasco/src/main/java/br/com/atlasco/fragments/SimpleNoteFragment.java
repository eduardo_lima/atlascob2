package br.com.atlasco.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import br.com.atlasco.R;
import br.com.atlasco.db.actions.DoNewNote;
import br.com.atlasco.db.model.Job;
import br.com.atlasco.db.utils.DBException;

/**
 * Created by Eduardo on 21/07/2015.
 */
@SuppressLint("ValidFragment")
public class SimpleNoteFragment extends DialogFragment{

    // Current job
    private final long mJobId;
    private final TabNotesFragment mHost;

    /**
     * Initialize job instance
     *
     * @param jobId
     * @param mHost
     */
    public SimpleNoteFragment(long jobId, TabNotesFragment mHost) {
        mJobId = jobId;
        this.mHost = mHost;

        try {
            // Instantiate the NoticeDialogListener so we can send events to the
            // host
            mListener = (NoticeDialogListener) mHost;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(mHost.getClass().getSimpleName() + " must implement NoticeDialogListener");
        }
    }

    /*
	 * The activity that creates an instance of this dialog fragment must
	 * implement this interface in order to receive event callbacks. Each method
	 * passes the DialogFragment in case the host needs to query it.
	 */
    public interface NoticeDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog);
    }

    // Use this instance of the interface to deliver action events
    NoticeDialogListener mListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        final View dialogView = inflater.inflate(R.layout.dialog_add_note, null);

        builder.setView(dialogView)
                // Add action buttons
                .setPositiveButton(R.string.dialog_positive_button, null).setNegativeButton(R.string.dialog_negative_button, null);

        final AlertDialog alertDialog = builder.create();
// Set this listener to set other listener for add task button
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {

                Button submit = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);

                // Sets the behavior when positive button is clicked
                submit.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {

                        try {

                            EditText noteDescription = (EditText) dialogView.findViewById(R.id.note_description);

                            // Create the new note for the job
                            DoNewNote dnn = new DoNewNote(getActivity().getApplicationContext(), mJobId, noteDescription.getText().toString());
                            dnn.run();

                            Toast.makeText(getActivity(), R.string.message_note_added, Toast.LENGTH_SHORT).show();

                            // Host must refresh
                            mListener.onDialogPositiveClick(SimpleNoteFragment.this);
                        } catch (DBException e) {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
        });

        return alertDialog;
    }
}
