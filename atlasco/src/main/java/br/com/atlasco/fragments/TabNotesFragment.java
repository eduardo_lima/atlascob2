package br.com.atlasco.fragments;

import android.app.DialogFragment;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.melnykov.fab.FloatingActionButton;

import br.com.atlasco.R;
import br.com.atlasco.activities.MainActivity;
import br.com.atlasco.adapters.NotesListViewAdapter;
import br.com.atlasco.db.actions.DoGetAllNotes;
import br.com.atlasco.db.utils.DBException;


/**
 * Tab for adding notes to a specifed job
 * <p>
 * Created by Eduardo on 18/07/2015.
 */
public class TabNotesFragment extends ListFragment implements SimpleNoteFragment.NoticeDialogListener {

    private NotesListViewAdapter mNotesListViewAdapter;
    private long mJobId;

    private DialogFragment mDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Get intent value
        mJobId = getActivity().getIntent().getIntExtra(MainActivity.VALUE_JOB_ID, 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_notes, container, false);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        try {
            FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
            fab.attachToListView(getListView());

            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mDialog = new SimpleNoteFragment(mJobId, TabNotesFragment.this);
                    mDialog.show(getActivity().getFragmentManager(), SimpleNoteFragment.class.getSimpleName());
                }
            });

            // Bind data for listview
            DoGetAllNotes dgan = new DoGetAllNotes(getActivity(), mJobId);

            dgan.run();

            mNotesListViewAdapter = new NotesListViewAdapter(getActivity(), getListView().getId(), dgan.getmList());

            getListView().setAdapter(mNotesListViewAdapter);
        } catch (DBException e) {

        }
    }

    private void refreshListView() {
        try {
            // DB call
            DoGetAllNotes dgan = new DoGetAllNotes(getActivity(), mJobId);

            dgan.run();

            mNotesListViewAdapter.addAll(dgan.getmList());

        } catch (DBException e) {
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {

        mDialog.dismiss();

        this.refreshListView();
    }
}
