package br.com.atlasco.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.joda.time.DateTime;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Seconds;

import java.text.DecimalFormat;
import java.util.List;

import br.com.atlasco.R;
import br.com.atlasco.activities.MainActivity;
import br.com.atlasco.db.actions.DoGetAllLocation;
import br.com.atlasco.db.actions.DoGetAllMarking;
import br.com.atlasco.db.actions.DoGetAllTrack;
import br.com.atlasco.db.actions.DoGetJob;
import br.com.atlasco.db.model.Job;
import br.com.atlasco.db.model.Location;
import br.com.atlasco.db.model.Marking;
import br.com.atlasco.db.model.Track;
import br.com.atlasco.db.utils.DBException;
import br.com.atlasco.enums.ActionEnum;

/**
 * Created by home on 05/06/2015.
 */
public class TabStatisticsFragment extends Fragment {

    private final static String TAG = TabStatisticsFragment.class.getSimpleName();

    private Job mSelectedJob;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tab_statistics, container, false);

        // Init all dynamic values for the view
        float departureDistance = 0.0f;
        float returnDistance = 0.0f;
        DateTime dtDepartureStart = new DateTime();
        DateTime dtDepartureEnd = new DateTime();
        DateTime dtReturnStart = new DateTime();
        DateTime dtReturnEnd = new DateTime();

        // Get intent value
        int id = getActivity().getIntent().getIntExtra(MainActivity.VALUE_JOB_ID, 0);

        try {
            // Get Job instance
            DoGetJob dgj = new DoGetJob(getActivity(), id);
            dgj.run();
            mSelectedJob = dgj.getmJob();

            // Get tracks of the job
            DoGetAllTrack dgat = new DoGetAllTrack(getActivity(), mSelectedJob.getId());
            dgat.run();
            List<Track> tracks = dgat.getmList();

            boolean isFirst = true;
            // Calculate distance
            for (Track t : tracks) {

                float trackDistance = 0.0f;

                DoGetAllLocation dgal = new DoGetAllLocation(getActivity(), t.getId());
                dgal.run();
                List<Location> locations = dgal.getmList(); // locatiosn
                // retrieved

                if (locations.size() > 0) { // secure

                    Location previous = locations.get(0);

                    for (Location l : locations) {
                        android.location.Location x = new android.location.Location("point x");
                        x.setLatitude(previous.getLatitude());
                        x.setLongitude(previous.getLongitude());

                        android.location.Location y = new android.location.Location("point y");
                        y.setLatitude(l.getLatitude());
                        y.setLongitude(l.getLongitude());

                        // in meters
                        trackDistance += x.distanceTo(y);
                        previous = l;
                    }

                    // it'd interesting to have a field in location table that'd say whether the track is of departure or return
                    if (isFirst) {
                        departureDistance = trackDistance;
                        isFirst = false;
                    } else {
                        returnDistance = trackDistance;
                    }
                }
            }

            DoGetAllMarking dgam = new DoGetAllMarking(getActivity(), mSelectedJob.getId());
            dgam.run();
            List<Marking> markings = dgam.getmList();

            for (Marking m : markings) {
                if (ActionEnum.MARK_GO_CUSTOMER.getValue() == m.getActionId()) {
                    dtDepartureStart = new DateTime(m.getCreatedAt());
                }
                if (ActionEnum.MARK_GO_START_SERVICE.getValue() == m.getActionId()) {
                    dtDepartureEnd = new DateTime(m.getCreatedAt());
                }
                if (ActionEnum.MARK_GO_END_SERVICE.getValue() == m.getActionId()) {
                    dtReturnStart = new DateTime(m.getCreatedAt());
                }
                if (ActionEnum.MARK_GO_HOME.getValue() == m.getActionId()) {
                    dtReturnEnd = new DateTime(m.getCreatedAt());
                }
            }

        } catch (DBException e) {
            Toast.makeText(getActivity(), R.string.message_problems_when_loading_job, Toast.LENGTH_SHORT).show();
        }

        // Set it on the view
        TextView textViewTotalDistance = (TextView) v.findViewById(R.id.metric_total_distance);
        textViewTotalDistance.setText(new DecimalFormat("0.00").format((departureDistance + returnDistance) / 1000) + " km");
        TextView textViewDepartureDistance = (TextView) v.findViewById(R.id.metric_departure_distance);
        textViewDepartureDistance.setText(new DecimalFormat("0.00").format(departureDistance / 1000) + " km");
        TextView textViewReturnDistance = (TextView) v.findViewById(R.id.metric_return_distance);
        textViewReturnDistance.setText(new DecimalFormat("0.00").format(returnDistance / 1000) + " km");

        TextView textViewTotalTime = (TextView) v.findViewById(R.id.metric_total_time);
        textViewTotalTime.setText(Hours.hoursBetween(dtDepartureStart, dtReturnEnd).getHours() % 24 + "h" + Minutes.minutesBetween(dtDepartureStart, dtReturnEnd).getMinutes() % 60 + "m" + Seconds.secondsBetween(dtDepartureStart, dtReturnEnd).getSeconds() % 60 + "s");
        TextView textViewDepartureTime = (TextView) v.findViewById(R.id.metric_departure_time);
        textViewDepartureTime.setText(Hours.hoursBetween(dtDepartureStart, dtDepartureEnd).getHours() % 24 + "h" + Minutes.minutesBetween(dtDepartureStart, dtDepartureEnd).getMinutes() % 60 + "m" + Seconds.secondsBetween(dtDepartureStart, dtDepartureEnd).getSeconds() % 60 + "s");
        TextView textViewReturnTime = (TextView) v.findViewById(R.id.metric_return_time);
        textViewReturnTime.setText(Hours.hoursBetween(dtReturnStart, dtReturnEnd).getHours() % 24 + "h" + Minutes.minutesBetween(dtReturnStart, dtReturnEnd).getMinutes() % 60 + "m" + Seconds.secondsBetween(dtReturnStart, dtReturnEnd).getSeconds() % 60 + "s");

        return v;
    }

}
