package br.com.atlasco.activities;

import java.util.ArrayList;
import java.util.List;

import br.com.atlasco.R;
import br.com.atlasco.adapters.JobsListViewAdapter;
import br.com.atlasco.db.actions.DoDeleteJob;
import br.com.atlasco.db.actions.DoDeleteLocations;
import br.com.atlasco.db.actions.DoDeleteMarkings;
import br.com.atlasco.db.actions.DoDeleteTracks;
import br.com.atlasco.db.actions.DoGetAction;
import br.com.atlasco.db.actions.DoGetAllJob;
import br.com.atlasco.db.actions.DoGetAllMarking;
import br.com.atlasco.db.actions.DoGetJob;
import br.com.atlasco.db.actions.DoGetTrack;

import br.com.atlasco.db.actions.DoNewJob;
import br.com.atlasco.db.actions.DoNewMarking;
import br.com.atlasco.db.actions.DoNewTrack;
import br.com.atlasco.db.actions.DoUpdateJob;
import br.com.atlasco.db.model.Action;
import br.com.atlasco.db.utils.DBException;
import br.com.atlasco.db.model.Job;
import br.com.atlasco.db.model.Track;
import br.com.atlasco.enums.ActionEnum;
import br.com.atlasco.fragments.SimpleJobFragment;
import br.com.atlasco.services.TrackRecordingService;

import android.app.ActionBar;
import android.app.DialogFragment;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.Toast;


public class MainActivity extends ListActivity implements SimpleJobFragment.NoticeDialogListener {

    // Declare constants
    private static final String TAG = MainActivity.class.getSimpleName();

    public final static String VALUE_JOB_ID = "br.com.atlasco.activities.JOB_ID";

    public final static String VALUE_TRACK_ID = "br.com.atlasco.activities.TRACK_ID";

    public final static String REQUESTING_LOCATION_UPDATES_KEY = "requesting-location-updates-key";

    public final static String CURRENT_JOB_ID_KEY = "current-job-id-key";

    public final static String CURRENT_ACTION_ID_KEY = "current-action-id-key";

    public final static String CURRENT_TRACK_ID_KEY = "current-track-id-key";

    // Declare variables
    private ListView mListView;

    private JobsListViewAdapter mJobsListViewAdapter;

    private List<Job> mJobList = new ArrayList<>();

    private Job mCurrentJob;

    private Action mLastAction;

    private Track mCurrentTrack;

    private DialogFragment mDialog;

    private boolean mRequestingLocationUpdates;

    /**
     * Launch action
     */
    private void performAction() {

        if ((mLastAction.getId() == ActionEnum.MARK_NO_ACTION.getValue()) ||
            (mLastAction.getId() == ActionEnum.MARK_GO_HOME.getValue())){
            this.startJob();
        } else if (mLastAction.getId() == ActionEnum.MARK_GO_CUSTOMER.getValue()) {
            this.goStartService();
        } else if (mLastAction.getId() == ActionEnum.MARK_GO_START_SERVICE.getValue()) {
            this.finishService();
        } else if (mLastAction.getId() == ActionEnum.MARK_GO_END_SERVICE.getValue()) {
            this.goFinishJob();
        }

    }

    /**
     * Start a new job and recording time and distance elapsed
     */
    private void startJob() {

        try {
            // Create a new job with date format as a title
            DoNewJob dnj = new DoNewJob(getApplicationContext(), "Tarefa atual");
            dnj.run();

            // Get job created and set as the current job
            DoGetJob dgj = new DoGetJob(getApplicationContext(), dnj.getmGeneratedId());
            dgj.run();
            mCurrentJob = dgj.getmJob(); // Set current job

            // Create the first marking of the job
            DoNewMarking dnm = new DoNewMarking(getApplicationContext(), mCurrentJob.getId(), 1);
            dnm.run();

            // Create the correspondent track of the job
            DoNewTrack dnt = new DoNewTrack(getApplicationContext(), mCurrentJob.getId());
            dnt.run();

            // Get generated track to start getting coordinates
            updateCurrentTrackObject(dnt.getmGeneratedId());

            Toast.makeText(getApplicationContext(), getResources().getString(R.string.service_started), Toast.LENGTH_SHORT).show();

            this.refreshListView();

            // After all, it should get the start to get the location
            this.togglePeriodicLocationUpdates();

            this.updateLastActionObject(1L);

        } catch (DBException e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (IllegalArgumentException e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Start record a new track for returning way
     */
    private void finishService() {

        try {
            // Test if there is two actions from currentJob
            DoGetAllMarking dgm = new DoGetAllMarking(getApplicationContext(), mCurrentJob.getId());
            dgm.run();
            if (dgm.getmList().size() != 2) {
                throw new IllegalArgumentException(getString(R.string.message_action_not_allowed));
            }

            // create a new marking with action 3
            DoNewMarking dnm = new DoNewMarking(getApplicationContext(), mCurrentJob.getId(), 3);
            dnm.run();

            // update job status
            mCurrentJob.setStatus(getString(R.string.status_in_transit));
            DoUpdateJob duj = new DoUpdateJob(getApplicationContext(), mCurrentJob);
            duj.run();

            // Create the correspondent track of the job
            DoNewTrack dnt = new DoNewTrack(getApplicationContext(), mCurrentJob.getId());
            dnt.run();

            // Get generated track to start getting coordinates
            updateCurrentTrackObject(dnt.getmGeneratedId());

            this.refreshListView();

            // stop location updates
            this.togglePeriodicLocationUpdates();

            this.updateLastActionObject(3L);

        } catch (IllegalArgumentException e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (DBException e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void goFinishJob() {

        try {
            // Test if there is only one marking from currentJob
            DoGetAllMarking dgm = new DoGetAllMarking(getApplicationContext(), mCurrentJob.getId());
            dgm.run();
            if (dgm.getmList().size() != 3) {
                throw new IllegalArgumentException(getString(R.string.message_action_not_allowed));
            }

            // create a new marking with action 4
            DoNewMarking dnm = new DoNewMarking(getApplicationContext(), mCurrentJob.getId(), 4);
            dnm.run();

            // update job status
            mCurrentJob.setStatus(getString(R.string.status_job_done));
            DoUpdateJob duj = new DoUpdateJob(getApplicationContext(), mCurrentJob);
            duj.run();

            // Call for fragment to edit job description
            Job copy = new Job();
            copy.setId(mCurrentJob.getId());
            copy.setDescription(mCurrentJob.getDescription());
            copy.setCreatedAt(mCurrentJob.getCreatedAt());
            copy.setStatus(mCurrentJob.getStatus());
            mDialog = new SimpleJobFragment(copy);
            mDialog.show(getFragmentManager(), SimpleJobFragment.class.getSimpleName());

            this.refreshListView();

            // stop location updates
            this.togglePeriodicLocationUpdates();

            this.updateLastActionObject(4L);

            // erase current job and track
            mCurrentJob = null;
            mCurrentTrack = null;

        } catch (IllegalArgumentException e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (DBException e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * Stop recording track, distance and running time
     */
    private void goStartService() {

        try {
            // Test if there is only one marking from currentJob
            DoGetAllMarking dgm = new DoGetAllMarking(getApplicationContext(), mCurrentJob.getId());
            dgm.run();
            if (dgm.getmList().size() != 1) {
                throw new IllegalArgumentException(getString(R.string.message_action_not_allowed));
            }

            // create a new marking with action 2
            DoNewMarking dnm = new DoNewMarking(getApplicationContext(), mCurrentJob.getId(), 2);
            dnm.run();

            // update job status
            mCurrentJob.setStatus(getString(R.string.status_at_customer));
            DoUpdateJob duj = new DoUpdateJob(getApplicationContext(), mCurrentJob);
            duj.run();

            this.refreshListView();

            // stop location updates
            this.togglePeriodicLocationUpdates();

            this.updateLastActionObject(2L);

        } catch (IllegalArgumentException e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        } catch (DBException e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set color for action bar
        ActionBar actionBar = getActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.color_primary)));

        mRequestingLocationUpdates = false;

        // Update values using data stored in the Shared preferences API.
        updateValuesFromSharedPreferences();

        // Não consigo entender pq não está pegando oq foi salvo, pq é salvo algo

        // Get listview widget
        mListView = getListView();

        try {
            mJobList.clear();

            // Get all available jobs
            DoGetAllJob dgaj = new DoGetAllJob(getApplicationContext());

            dgaj.run();

            mJobList = dgaj.getmList();

            // Pass results to JobsListViewAdapter class
            mJobsListViewAdapter = new JobsListViewAdapter(this, mListView.getId(), mJobList);

            // Binds the adapter to the ListView
            mListView.setAdapter(mJobsListViewAdapter);
            // Selector for listview
            mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
            // Capture ListView item click
            mListView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
                                                     @Override
                                                     public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
                                                         // Capture total checked items
                                                         final int checkedCount = mListView.getCheckedItemCount();
                                                         // Set the CAB title according to total checked items
                                                         mode.setTitle(checkedCount + " selecionados");
                                                         // Calls toggleSelection method from JobsListViewAdapter class
                                                         mJobsListViewAdapter.toggleSelection(position);
                                                     }

                                                     @Override
                                                     public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                                                         mode.getMenuInflater().inflate(R.menu.context_action_menu_main, menu);
                                                         return true;
                                                     }

                                                     @Override
                                                     public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                                                         return false;
                                                     }

                                                     @Override
                                                     public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                                                         switch (item.getItemId()) {
                                                             case R.id.action_remove_job:
                                                                 // Calls getSelectedIds method from JobsListViewAdapter Class
                                                                 SparseBooleanArray selected = mJobsListViewAdapter.getSelectedIds();
                                                                 // Captures all selected ids with a loop
                                                                 for (int i = (selected.size() - 1); i >= 0; i--) {
                                                                     if (selected.valueAt(i)) {
                                                                         Job selectedItem = mJobsListViewAdapter.getItem(selected.keyAt(i));

                                                                         try {
                                                                             DoDeleteMarkings ddm = new DoDeleteMarkings(getApplicationContext(), selectedItem.getId());
                                                                             ddm.run();

                                                                             DoDeleteLocations ddl = new DoDeleteLocations(getApplicationContext(), selectedItem.getId());
                                                                             ddl.run();

                                                                             DoDeleteTracks ddt = new DoDeleteTracks(getApplicationContext(), selectedItem.getId());
                                                                             ddt.run();

                                                                             DoDeleteJob ddj = new DoDeleteJob(getApplicationContext(), selectedItem.getId());
                                                                             ddj.run();

                                                                             // Remove selected items following the ids
                                                                             mJobsListViewAdapter.remove(selectedItem);
                                                                         } catch (DBException e) {
                                                                             Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                                                         }
                                                                     }
                                                                 }
                                                                 // Close CAB
                                                                 mode.finish();
                                                                 return true;
                                                             default:
                                                                 return false;
                                                         }
                                                     }

                                                     @Override
                                                     public void onDestroyActionMode(ActionMode mode) {
                                                         mJobsListViewAdapter.removeSelection();
                                                     }
                                                 }

            );

        } catch (DBException e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Updates fields on data stored in activity shared preferences file
     */
    private void updateValuesFromSharedPreferences(){
        Log.i(TAG, "Update values from shared preferences");

        SharedPreferences settings = getPreferences(Context.MODE_PRIVATE);

        if (settings.contains(REQUESTING_LOCATION_UPDATES_KEY)){
            mRequestingLocationUpdates = settings.getBoolean(REQUESTING_LOCATION_UPDATES_KEY, false);
        }

        if (settings.contains(CURRENT_JOB_ID_KEY)){
            updateCurrentJobFromSharedPrefsOrDB(settings.getLong(CURRENT_JOB_ID_KEY, 0));
        }

        if (settings.contains(CURRENT_ACTION_ID_KEY)){
            updateLastActionObject(settings.getLong(CURRENT_ACTION_ID_KEY, 0));
        } else {
            updateLastActionObject(5L);
        }

        if (settings.contains(CURRENT_TRACK_ID_KEY)){
            updateCurrentTrackObject(settings.getLong(CURRENT_TRACK_ID_KEY, 0));
        }
    }

    /**
     * Retrieve the current job id from a savedinstance or the last id generated, then assign the object.
     * The mCurrentJob will be set to null depending on the last job status
     *
     * @param id From saved instance
     */
    private void updateCurrentJobFromSharedPrefsOrDB(long id) {

        long jobIdFromList = (mJobList.size() > 0) ? mJobList.get(0).getId() : 0;
        long jobID = (id > 0) ? id : jobIdFromList;

        try {
            DoGetJob dgj = new DoGetJob(getApplicationContext(), jobID);

            dgj.run();

            Job topListJob = dgj.getmJob();

            if ((topListJob != null) && (!topListJob.getStatus().equals(getString(R.string.status_job_done)))) {
                mCurrentJob = topListJob;
            }
        } catch (DBException e) {
            Toast.makeText(getApplicationContext(), getString(R.string.message_problems_when_loading_job), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Retrieve the current action id from a savedinstance, then assign the object
     * The mCurrentAction always must have be assigned
     *
     * @param id Id for the action
     */
    private void updateLastActionObject(long id) {

        try {
            DoGetAction dga = new DoGetAction(getApplicationContext(), id);
            dga.run();

            mLastAction = dga.getmAction();

        } catch (DBException e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Retrieve the current track id from a savedinstance, then assign the object
     *
     * @param id Id of the track
     */
    private void updateCurrentTrackObject(long id) {
        try {
            DoGetTrack dgt = new DoGetTrack(getApplicationContext(), id);
            dgt.run();

            mCurrentTrack = dgt.getmTrack();

        } catch (DBException e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_perform_action:

                performAction();

                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog) {

        mDialog.dismiss();

        this.refreshListView();
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {

        Job job = mJobsListViewAdapter.getJobAtPosition(position);

        Intent i = new Intent(MainActivity.this, DetailsActivity.class);
        i.putExtra(VALUE_JOB_ID, job.getId());

        startActivity(i);
    }

    private void refreshListView() {
        try {
            // DB call
            DoGetAllJob dgaj = new DoGetAllJob(getApplicationContext());

            dgaj.run();

            mJobsListViewAdapter.addAll(dgaj.getmList());

        } catch (DBException e) {
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * Method to toggle periodic location updates
     */
    private void togglePeriodicLocationUpdates() {
        if (!mRequestingLocationUpdates) {

            mRequestingLocationUpdates = true;

            // Starting the location updates
            startTrackRecording();

            Log.d(TAG, "Periodic location updates started!");

        } else {

            mRequestingLocationUpdates = false;

            // Stopping the location updates
            stopTrackRecording();

            Log.d(TAG, "Periodic location updates stopped!");
        }
    }

    private void startTrackRecording() {
        Intent i = new Intent(MainActivity.this, TrackRecordingService.class);
        i.putExtra(VALUE_TRACK_ID, mCurrentTrack.getId()); // Preciso salvar o estado dos registros atuais
        startService(i);
    }

    private void stopTrackRecording() {
        Intent i = new Intent(MainActivity.this, TrackRecordingService.class);
        stopService(i);
    }

    @Override
    protected void onStop() {
        super.onStop();

        /**
         * Stores activity data in the Bundle.
         */
        SharedPreferences settings = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(REQUESTING_LOCATION_UPDATES_KEY, mRequestingLocationUpdates);
        editor.putLong(CURRENT_ACTION_ID_KEY, mLastAction.getId());
        editor.putLong(CURRENT_JOB_ID_KEY, (mCurrentJob != null) ? mCurrentJob.getId() : 0);
        editor.putLong(CURRENT_TRACK_ID_KEY, (mCurrentTrack != null) ? mCurrentTrack.getId() : 0);
        editor.commit();
    }
}
