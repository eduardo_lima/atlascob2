package br.com.atlasco.activities;

import br.com.atlasco.R;
import br.com.atlasco.adapters.ViewPagerAdapter;
import br.com.atlasco.db.actions.DoGetAllLocation;
import br.com.atlasco.db.actions.DoGetAllTrack;
import br.com.atlasco.db.actions.DoGetJob;
import br.com.atlasco.db.model.Job;
import br.com.atlasco.db.model.Location;
import br.com.atlasco.db.model.Track;
import br.com.atlasco.db.utils.DBException;
import br.com.atlasco.external.SlidingTabLayout;

import android.app.Fragment;
import android.graphics.Camera;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.text.DecimalFormat;
import java.util.List;

public class DetailsActivity extends ActionBarActivity implements OnMapReadyCallback {

    private static final String TAG = DetailsActivity.class.getSimpleName();

    // Objects for tab view
    private Toolbar toolbar;
    private ViewPager pager;
    private ViewPagerAdapter adapter;
    private SlidingTabLayout tabs;
    private int numberOfTabs = 3;

    private int mJobID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_info);

        // Creating The Toolbar and setting it as the Toolbar for the activity
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        CharSequence[] titles = {getString(R.string.title_tab_map_track), getString(R.string.title_tab_statistics), getString(R.string.title_tab_notes)};
        adapter = new ViewPagerAdapter(getSupportFragmentManager(), titles, numberOfTabs);

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.color_tab_scroll_bar);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);

        // Get intent value
        mJobID = getIntent().getIntExtra(MainActivity.VALUE_JOB_ID, 0);


        try {
            // Get Job instance
            DoGetJob dgj = new DoGetJob(getApplicationContext(), mJobID);
            dgj.run();

            // Show job description on actionbar view
            getSupportActionBar().setTitle(dgj.getmJob().getDescription());


        } catch (DBException e) {
            Toast.makeText(getApplicationContext(), R.string.message_problems_when_loading_job, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        try {
            int colorCode = Color.GREEN; // Set the first color
            // To fit bounds
            LatLngBounds.Builder builder = new LatLngBounds.Builder();

            boolean hasLatLng = false;

            // Desenhar polyline do trajeto de ida
            DoGetAllTrack dgat = new DoGetAllTrack(getApplicationContext(), mJobID);

            dgat.run();

            List<Track> tracks = dgat.getmList();

            for (Track t : tracks) {

                PolylineOptions polylineOptions = new PolylineOptions();

                DoGetAllLocation dgal = new DoGetAllLocation(getApplicationContext(), t.getId());
                dgal.run();

                List<Location> locations = dgal.getmList();

                for (Location l : locations) {
                    LatLng lng = new LatLng(l.getLatitude(), l.getLongitude());
                    polylineOptions.add(lng);

                    builder.include(lng);
                    hasLatLng = true;
                }

                if (!locations.isEmpty()) {
                    Polyline polyline = googleMap.addPolyline(polylineOptions);
                    polyline.setColor(colorCode);

                    colorCode = Color.RED; // Set the second color
                }
            }

            CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(-26.9211621, -49.1249323));

            googleMap.animateCamera(center);

            if (hasLatLng) {
                LatLngBounds bounds = builder.build();

                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 0);

                googleMap.animateCamera(cu);
            }
        } catch (DBException e) {
            Log.d(TAG, "shhhhhhhhhhhh");
        }
    }
}
