package br.com.atlasco.activities.listeners;

import br.com.atlasco.db.actions.DoNewLocation;
import br.com.atlasco.db.model.Track;
import br.com.atlasco.db.utils.DBException;
import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

@Deprecated
public class StoreLocationListener implements android.location.LocationListener {

	private static final String TAG = StoreLocationListener.class.getSimpleName();

	private final Context mContext;

	private final Track mTrack;

	/**
	 * Set the context to store location and the track register
	 * 
	 * @param c
	 */
	public StoreLocationListener(Context c, Track t) {
		this.mContext = c;
		this.mTrack = t;
	}

	@Override
	public void onLocationChanged(Location location) {
		try {
			// Assign the new location and save it
			DoNewLocation dnc = new DoNewLocation(mContext, mTrack.getId(), location.getLatitude(), location.getLongitude());
			dnc.run();

			Log.d(TAG, "Location stored!");
		} catch (DBException e) {
			Log.e(TAG, e.getMessage()); // dont show toast kozz it's a loop
		}
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		Log.d(TAG, "status changed");
	}

	@Override
	public void onProviderEnabled(String provider) {
		Log.d(TAG, "provider enabled");
	}

	@Override
	public void onProviderDisabled(String provider) {
		Log.d(TAG, "provider disabled");
	}

}
