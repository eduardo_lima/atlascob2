package br.com.atlasco.activities.tasks;

import br.com.atlasco.db.model.Track;

import android.content.Context;
import android.location.LocationManager;
import android.os.AsyncTask;

@Deprecated
public class StoreLocationTask extends AsyncTask<String, Integer, String> {

    private static int DISPLACEMENT = 10; // 10 meters

    private static int UPDATE_INTERVAL = 10000; // 10 sec

    private final Context mContext;

    private final Track mTask;

    private LocationManager mService;

    private br.com.atlasco.activities.listeners.StoreLocationListener mListener;

    public StoreLocationTask(Context c, Track t) {
        mContext = c;
        mTask = t;
    }

    @Override
    protected void onPreExecute() {

        mListener = new br.com.atlasco.activities.listeners.StoreLocationListener(mContext, mTask);

        mService = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        mService.requestLocationUpdates(LocationManager.GPS_PROVIDER, UPDATE_INTERVAL, DISPLACEMENT, mListener);
    }

    @Override
    protected String doInBackground(String... params) {

        float dummy = 0.0f;

        while (dummy == 0.0) {
            if (isCancelled()) {
                break;
            }
        }

        return null;
    }

    @Override
    protected void onCancelled() {
        mService.removeUpdates(mListener);
    }
}
